/* eslint-disable no-case-declarations */
import Component from './component';

export default class NewTable extends Component {
  constructor($element) {
    super($element);
    // duplicate source of different editor elements
    this.dataFrom = this.target.parentNode.getAttribute('data-from');
    this.dataSources = this.dataFrom ? this.dataFrom.split(' ') : [];
    this.id = $element.getAttribute('id');
    this.cache = {
      tasks: [],
    };

    this.init();
  }

  init() {
    const autofill = this.target.parentNode.getAttribute('data-autofill');

    switch (autofill) {
      case 'columns':
        this._columnManager();
        break;
      case 'rows':
        this._rowManager();
        break;
      case 'cells':
        const allCells = this.target.querySelectorAll('.table-new__cell');

        allCells.forEach(($cell) => {
          const columnId = $cell.getAttribute('data-column-id');
          const rowId = $cell.getAttribute('data-row-id');
          // add listener only for cells in table
          if (columnId && rowId) {
            this._cellManager($cell, columnId, rowId);
          }
        });
        break;
      default:
        console.log('NewTableERROR: Undefinated autofill key word!', autofill);
    }
  }

  _rowManager() {
    document.addEventListener('editor.change', (e) => {
      const data = e.detail.text;

      if (this.dataSources.includes(data.id)) {
        let $tableRow;
        let rightToAdd = true;

        // get columns header
        const $header = this.target.previousSibling;
        const $headerCells = $header.firstChild.childNodes;

        switch (data.task) {
          case 'add':
            $tableRow = document.createElement('div');
            $tableRow.setAttribute('class', 'table-new__row');

            $headerCells.forEach(($item) => {
              const $rowCell = document.createElement('div');
              $rowCell.setAttribute('class', 'table-new__cell');
              const itemType = JSON.parse($item.getAttribute('data-type'));

              // check if tagName is definated and than set variable to false
              if (itemType.tagName && itemType.tagName !== data.tagData.tagName) {
                rightToAdd = false;
              }
              else {
                // add data to cell
                this.fillCell(itemType, data, $rowCell);
              }

              // only add if it is right to add
              if (rightToAdd) $tableRow.appendChild($rowCell);
            });

            // only add if it is right to add
            if (rightToAdd) this.target.appendChild($tableRow);
            break;
          case 'remove':
            const tableRows = this.target.childNodes;

            tableRows.forEach(($row) => {
              $row.childNodes.forEach(($cell, index) => {
                const itemType = JSON.parse($headerCells[index].getAttribute('data-type'));

                if (NewTable.isRightToDeleteHelper($cell, data.value, itemType)) {
                  $tableRow = $row;

                }
              });
            });

            // olny if row for remove exists
            if ($tableRow) {
              // remove tasks from cache
              $tableRow.childNodes.forEach(($cell) => {
                this._removeTaskFromCache($cell);
              });

              $tableRow.remove();
            }
            break;
          default:
            console.log('NewTableERROR: Undefinated task for new table! (rowManager)');
        }
      }
    });
  }

  // TODO: adding just specific tags names like in rowManager
  _columnManager() {
    document.addEventListener('editor.change', (e) => {
      const data = e.detail.text;

      if (this.dataSources.includes(data.id)) {
        const $rows = this.target.childNodes;

        switch (data.task) {
          case 'add':
            $rows.forEach(($row) => {
              const $cell = document.createElement('div');
              $cell.setAttribute('class', 'table-new__cell');

              const $firstCell = $row.firstChild;
              const itemType = JSON.parse($firstCell.getAttribute('data-type'));

              // add data to cell
              this.fillCell(itemType, data, $cell);

              $row.appendChild($cell);
            });
            break;
          case 'remove':
            let columnIndex;
            $rows.forEach(($row) => {
              const $firstCell = $row.firstChild;
              const itemType = JSON.parse($firstCell.getAttribute('data-type'));

              $row.childNodes.forEach(($cell, index) => {
                if (NewTable.isRightToDeleteHelper($cell, data.value, itemType)) {
                  columnIndex = index;
                }
              });
            });

            this.target.childNodes.forEach(($row) => {
              // remove tasks from cache
              this._removeTaskFromCache($row.childNodes[columnIndex]);

              $row.childNodes[columnIndex].remove();
            });
            break;
          default:
            console.log('NewTableERROR: Undefinated task for new table! (columnManager)');
        }
      }
    });
  }

  _cellManager($cell, columnId, rowId) {
    const itemType = JSON.parse(this._findCellType($cell));
    const cellController = (data) => {
      switch (data.task) {
        case 'add':
          const $tag = document.createElement('div');
          this.fillCell(itemType, data, $cell);

          $cell.appendChild($tag);
          break;
        case 'remove':
          const $tags = $cell.querySelectorAll('div');
          let removed = false;
          for (let i = 0; i < $tags.length; i += 1) {
            if (!removed && NewTable.isRightToDeleteHelper($tags[i], data.value, itemType)) {
              // TODO: remove only childrens of $elm => empty div is still there
              $tags[i].remove();
              removed = true;
            }
          }
          break;
        default:
          console.log('NewTableERROR: Undefinated task for new table! (cellManager)');
      }
    };

    document.addEventListener('editor.change', (e) => {
      const data = e.detail.text;

      if (this.dataSources.includes(data.id) && (data.tagData.tagName === rowId || data.tagData.tagName === columnId)) {

        if (!itemType.textId || itemType.textId === data.id) {
          cellController(data);
        }
      }
    });
    document.addEventListener('draggable.drop', (e) => {
      const data = e.detail;

      if (this.dataSources.includes(data.id) && e.detail.columnId === columnId) {
        // get text
        const $span = data.$node.querySelector('span').cloneNode(true);
        $span.classList.add('node-selected', 'node-text-gray');
        $span.setAttribute('data-by', 'table-drop');

        cellController({ task: data.task, value: $span });
      }
    });
  }

  fillCell(itemType, data, $node) {
    let $innerItem;
    let cacheObj;

    // seed init or generate
    let seed = 0;
    if (this.cache.tasks.length > 0) {
      const lastId = this.cache.tasks[this.cache.tasks.length - 1].id;
      const idParts = lastId.split('-');
      seed = +idParts[idParts.length - 1] + 1;
    }

    $node.classList.add(`table-new__cell--${itemType.name}`, `table-new__cell--${itemType.type}`);

    switch (itemType.name) {
      case 'dup-text':
        $innerItem = document.createElement('div');
        if (itemType.type === 'text') {
          $innerItem.innerHTML = data.value.textContent;
        }
        else if (itemType.type === 'tag') {
          // have to by clone, because if not it will by duplicatet do more tables it will be just in last one
          $innerItem.appendChild(data.value.cloneNode(true));
        }
        break;
      case 'select':
        // create task in cache
        cacheObj = { id: `${this.id}-select-${seed}`, answer: '', rightAnswer: data.tagData };
        this.cache.tasks.push(cacheObj);

        // create node in html
        $innerItem = document.createElement(itemType.name);

        $innerItem.setAttribute('id', cacheObj.id);
        $innerItem.setAttribute('data-save', 'value');
        $innerItem.setAttribute('tabindex', -1);

        itemType.options.forEach((value) => {
          const $option = document.createElement('option');
          $option.innerHTML = value;
          $innerItem.appendChild($option);
        });

        // add event listener
        $innerItem.addEventListener('change', (event) => this._correctedAnswer(event, cacheObj));
        break;
      case 'input':
      case 'textarea':
        // create task in cache
        cacheObj = { id: `${this.id}-input-${seed}`, answer: '', rightAnswer: data.tagData };
        this.cache.tasks.push(cacheObj);

        // create node in html
        $innerItem = document.createElement(itemType.name);

        $innerItem.setAttribute('id', cacheObj.id);
        $innerItem.setAttribute('data-save', 'value');
        $innerItem.setAttribute('tabindex', -1);

        if (itemType.name === 'textarea') {
          $innerItem.setAttribute('resize', cacheObj.id);
        }

        Object.keys(itemType.attributes).forEach((key) => {
          $innerItem.setAttribute(key, itemType.attributes[key]);
        });

        // add event listener
        $innerItem.addEventListener('change', (event) => this._correctedAnswer(event, cacheObj));
        break;
      case 'drop':
        $innerItem = document.createElement('div');
        const numberOfDrops = itemType.number ? itemType.number : -1;
        // set attribute for draggable logic
        $innerItem.setAttribute('data-drop-place', numberOfDrops);
        break;
      default:
        $innerItem = document.createElement('div');
        /*
          if item Type name is value of data object from delivered cviceni feature
          then use it and it's value set in table
          else set empry value and show error
       */
        if (data.tagData[itemType.name]) {
          $innerItem.innerHTML = data.tagData[itemType.name];
        }
        else {
          $innerItem.innerHTML = ' ';
          console.log('FillCellERROR: Undefinated object name.', itemType);
        }
    }
    $node.appendChild($innerItem);
  }

  // prepare for more types of duplicate types
  static isRightToDeleteHelper(elm, deliveredValue, itemType) {
    let result;

    if (itemType.name === 'dup-text') {
      // fix for tags
      // table div textContent include '×' as last char
      // because it is in self span
      let elemTextContent;
      if (itemType.type === 'tag' && elm.textContent[elm.textContent.length - 1] === '×') {
        elemTextContent = elm.textContent.slice(0, -1);
      }
      else {
        elemTextContent = elm.textContent;
      }
      result = elemTextContent === deliveredValue.textContent;

    } // different type then dup-text hasn't duplicate option
    else {
      result = false;
    }

    return result;
  }

  _findCellType($cell) {
    const rowType = $cell.parentNode.firstChild.getAttribute('data-type');
    if (rowType) return rowType;

    // if rows header isn´t definated
    const columnId = $cell.getAttribute('data-column-id');
    const $columnHeader = this.target.parentNode.querySelector(`.table-new__cell--header[data-column-id=${columnId}]`);
    if ($columnHeader) return $columnHeader.getAttribute('data-type');

    return 'empty';
  }

  _correctedAnswer(event, cacheObj) {
    this.cache.tasks.forEach((task) => {
      if (task.id === cacheObj.id) {
        task.answer = event.target.value;
      }
    });

    // console.log('select change', this.cache);
  }

  _removeTaskFromCache($cell) {
    this.cache.tasks.forEach((task, index) => {
      if ($cell.firstChild.tagName === 'SELECT' || $cell.firstChild.tagName === 'INPUT' || $cell.firstChild.tagName === 'TEXTAREA') {
        if ($cell.firstChild.id === task.id) {
          this.cache.tasks.splice(index, 1);
        }
      }
    });
  }
}
